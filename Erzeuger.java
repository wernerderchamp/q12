import java.time.LocalDateTime; 
import java.time.format.DateTimeFormatter; 

class Erzeuger implements Runnable{   
    // Attribute Anfang
    private Speicher speicher;
    private int lfdNr = 0;
    // Attribute Ende
    // -------------------------------
    // Konstruktoren Anfang
    public Erzeuger(Speicher speicher){
        this.speicher = speicher;
    }
    // Konstruktoren Ende
    // -------------------------------
    // Methoden Anfang
    public void arbeite(){        
        while(true){
            speicher.legeKisteAb(this.produziere()); // zeitkritisch -> wait() & notify()
            System.out.println("Kiste in den Speicher eingelagert!");
        }
    }
    
    public Kiste produziere(){
        this.lfdNr++;
        System.out.println("Neue Kiste erzeugt. LfdNr: " + lfdNr);
        LocalDateTime now = LocalDateTime.now(); 
        DateTimeFormatter df; 
        df = DateTimeFormatter.ISO_DATE_TIME;     // 2018-02-02T11:05:41.337 
        System.out.println(now.format(df));        
        return (new Kiste(this.lfdNr));
    }
    
    public void run(){
        this.arbeite();
    }
    // Methoden Ende
    // -------------------------------
}