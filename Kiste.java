class Kiste{   
    // Attribute Anfang
    private int idNr;
    // Attribute Ende
    // -------------------------------
    // Konstruktoren Anfang
    public Kiste(int idNr){
        this.idNr = idNr;
    }
    // Konstruktoren Ende
    // -------------------------------
    // Methoden Anfang
    public void gebeInfosAus(){
        System.out.println("Meine Id lautet: " + this.idNr);
    }
    // Methoden Ende
    // -------------------------------
}