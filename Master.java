
/**
 * Beschreiben Sie hier die Klasse Hauotklasse.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Master
{
    public static void main(){
        System.out.println("Jetzt gehts los!");
        Speicher speicher = new Speicher();
        Erzeuger erzeuger = new Erzeuger(speicher);
        Verbraucher verbraucher= new Verbraucher(speicher);
        System.out.println("Nun werden die Threads erzeugt!");
        Thread threadi1 = new Thread(erzeuger);
        Thread threadi2 = new Thread(verbraucher);
        System.out.println("Jetzt wird threadi1 gestartet - Erzeuger");
        threadi1.start();
        System.out.println("Jetzt wird threadi2 gestartet - Verbraucher");
        threadi2.start();        
    }
}
