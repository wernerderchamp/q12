//Mathe Abi wird einfach

public class MatheIstUnlogisch
{
   
  public static void main(String... args) {
    double two = two();
    System.out.format("Variable two = %.15f%n", two);
    double four = Math.ceil(two + two); // für den Fall mal runden
    System.out.format("two + two = %.15f%n", four);
  }

  // 20 * .1 = 2
  private static double two() {
    double two = 0;
    for(int i = 0; i < 20; i++) {
      two += .1;
    }
    return two;//20*0,1=2 ist ja klar
  }

}