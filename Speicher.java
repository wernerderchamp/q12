class Speicher{
    // Attribute Anfang
    private boolean frei;
    private Kiste[] lagerhalle;
    // Attribute Ende
    // -------------------------------
    // Konstruktoren Anfang
    public Speicher(){
        this.lagerhalle = new Kiste[5];
    }
    // Konstruktoren Ende
    // -------------------------------
    // Methoden Anfang
    
    public synchronized boolean kisteEinlagern(Kiste kiste){
        int platz=0;
        try{
            while(lagerhalle[platz]!=null) platz++;
            lagerhalle[platz]=kiste;
            return true;
        } catch (OutOfBoundsExeption e){
            return false;
        }
    }
    
    public synchronized void legeKisteAb(Kiste kiste){
        while(!this.frei){
            System.out.println("Ich (der Speicher) bin besetzt, kann also keine Kiste aufnehmen");
            try{
                this.wait();
            }
            catch(Exception e){
                // Tue nichts mit Exception
            }
        }
        this.frei = false;
        this.notify();
    }

    public synchronized Kiste gebeKisteAus(){
        while(this.frei){
            System.out.println("Ich bin frei, kann also keine Kiste abgeben");
            try{
                this.wait();
            }
            catch(Exception e){
                // Tue nichts mit Exception
            }
        }
        Kiste kisteLok = this.kiste;
        kiste = null;
        this.frei =true;
        this.notify();
        return kisteLok; 
    }
    // Methoden Ende
    // -------------------------------
}