import java.util.Random;

class Verbraucher implements Runnable{   
    // Attribute Anfang
    private Speicher speicher;
    private final int ZEITFENSTER = 1;
    private Random zufallsObjekt;
    // Attribute Ende
    // -------------------------------
    // Konstruktoren Anfang
    public Verbraucher(Speicher speicher){
        this.zufallsObjekt = new Random();
        this.speicher = speicher;
    }
    // Konstruktoren Ende
    // -------------------------------
    // Methoden Anfang
    public void arbeite(){
        while(true){
            this.lagereKisteEin(this.speicher.gebeKisteAus()); // Zeitkritisch -> wait() & notify()
            try{
                Thread.sleep((zufallsObjekt.nextInt(1000)+500)*ZEITFENSTER);
            }
            catch (InterruptedException e) {
                System.out.println("Exception in thread: "+ e.getMessage());
            }
            System.out.println("Kiste ins Lager!");
        }
    }

    public void lagereKisteEin(Kiste kiste){
        kiste.gebeInfosAus();
    }

    public void run(){
        this.arbeite();
    }
    // Methoden Ende
    // -------------------------------
}